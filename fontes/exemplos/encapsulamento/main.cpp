#include <QCoreApplication>



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    return a.exec();
}























































class Conta {
public:
    double getSaldo() const { return saldo; }

    bool temSaldo() const {
        return saldo > 0.0;
    }

private:
    double saldo;
};

class OperacoesConta {
public:
    bool calcularDesconto() {
        double saldo = conta.getSaldo();
        return saldo * 0.01;
    }

private:
    Conta conta;
};
