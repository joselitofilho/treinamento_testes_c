#include <QCoreApplication>

class Estado {

};

class MaquinaEstado {
public:
    virtual void mudarEstadoPara(Estado *estado) = 0;
};

class MaquinaEstadoPolibras : public MaquinaEstado {
public:
    void mudarEstadoPara(Estado *estado) {
        if (estado == NULL) {
            estado = new Estado();
        }
        //m_estado = estado;
    }
};

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    return a.exec();
}
