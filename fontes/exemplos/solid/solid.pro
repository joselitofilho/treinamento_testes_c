#-------------------------------------------------
#
# Project created by QtCreator 2015-03-05T20:48:21
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = solid
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp

HEADERS += \
    ocp_certo.z \
    ocp_errado.z \
    lsp.z \
    isp.z \
    dip_errado.z \
    dip_certo.h

DISTFILES +=
