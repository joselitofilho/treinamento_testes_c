#ifndef DIP_H
#define DIP_H

class IDesenvolvedor {
public:
    virtual void codifica() = 0;
};

class Desenvolvedor : public IDesenvolvedor {
public:
    void codifica() {
        // Codifica
    }
};

class Gerente {
public:
    void setDesenvolvedor(IDesenvolvedor dev) { m_dev = dev; }

    void supervisiona() {
        dev.codifica();
    }

private:
    IDesenvolvedor dev;
};

class SuperDesenvolvedor : public IDesenvolvedor {
public:
    void codifica() {
        // Codifica muito!
    }
};

#endif // DIP_H
