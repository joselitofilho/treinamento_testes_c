#include <QCoreApplication>
#include <QDebug>

class Conta {
public:
    Conta() :
        m_numero("4321"),
        m_temPendenciaCredito(false) {
    }

    Conta(const QString& numero, bool pendenciaCredito) :
        m_numero(numero),
        m_temPendenciaCredito(pendenciaCredito) {
    }

    const QString& getNumero() const { return m_numero; }

    bool temPendenciaCredito() const { return m_temPendenciaCredito; }
    void setTemPendenciaCredito(bool temPendencia) { m_temPendenciaCredito = temPendencia; }

    Conta *comPendenciaCredito(bool pendenciaCredito) {
        return new Conta(getNumero(), pendenciaCredito);
    }

private:
    QString m_numero;
    bool m_temPendenciaCredito;
};

class OperacoesConta {
public:
    Conta *processarPendenciaCredito(Conta *a_conta) {
        if (naoPagouAContaAposVencimento(a_conta)) {
            return a_conta->comPendenciaCredito(true);
        }

        return a_conta;
    }

private:
    bool naoPagouAContaAposVencimento(Conta *a_conta) {
        return true;
    }
};


int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Conta *conta = new Conta();
    qDebug() << "Tem pendencia? " << conta->temPendenciaCredito();

    OperacoesConta operacoesConta;
    operacoesConta.processarPendenciaCredito(conta);

    qDebug() << "Tem pendencia? " << conta->temPendenciaCredito();

    return a.exec();
}
