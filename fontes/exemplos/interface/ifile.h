#ifndef IFILE_H
#define IFILE_H

#include <QString>

class IFile {
public:
    virtual void open() = 0;
    virtual void write(const QString& message) = 0;
    virtual void close() = 0;
};

#endif // IFILE_H
