#include <QCoreApplication>
#include "cfile.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    CFile f("a.txt", "a");
    f.open();
    f.write("ze\n");
    f.close();

    return a.exec();
}
