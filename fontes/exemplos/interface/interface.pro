#-------------------------------------------------
#
# Project created by QtCreator 2015-03-02T18:58:48
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = interface
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    cfile.cpp

HEADERS += \
    cfile.h \
    ifile.h
