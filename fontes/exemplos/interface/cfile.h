#ifndef CFILE_H
#define CFILE_H

#include "ifile.h"

class CFile : public IFile {
public:
    CFile(QString m_fileName, QString m_mode);
    ~CFile();

    void open();
    void write(const QString& message);
    void close();

private:
    FILE *m_cfile;
    QString m_fileName;
    QString m_mode;
};

#endif // CFILE_H
