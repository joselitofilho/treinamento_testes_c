#include "cfile.h"
#include <QDebug>
#include <cstdio>

CFile::CFile(QString fileName, QString mode) :
    m_cfile(NULL),
    m_fileName(fileName),
    m_mode(mode)
{
}

CFile::~CFile()
{
    close();
    delete m_cfile;
    m_cfile = NULL;
}

void CFile::open()
{
    m_cfile = fopen(m_fileName.toStdString().c_str(), m_mode.toStdString().c_str());
}

void CFile::write(const QString &message)
{
    fprintf(m_cfile, message.toStdString().c_str());
}

void CFile::close()
{
    fclose(m_cfile);
}
