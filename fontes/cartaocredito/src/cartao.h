#ifndef CARTAO_H
#define CARTAO_H

#include <QString>

class Cartao
{
public:
    Cartao(const QString &numero);
    ~Cartao();

    const QString& numero() const { return m_numero; }

private:
    QString m_numero;

};

#endif // CARTAO_H

