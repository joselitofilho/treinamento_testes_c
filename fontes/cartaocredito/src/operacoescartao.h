#ifndef OPERACOESCARTAO_H
#define OPERACOESCARTAO_H

#include <QString>
#include <cartao.h>

class OperacoesCartao
{
public:
    OperacoesCartao();
    ~OperacoesCartao();

    QString qualBandeira(Cartao cartao);
};



#endif // OPERACOESCARTAO_H

