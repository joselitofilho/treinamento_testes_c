#include "operacoescartao.h"

OperacoesCartao::OperacoesCartao()
{

}

OperacoesCartao::~OperacoesCartao()
{

}

QString OperacoesCartao::qualBandeira(Cartao cartao)
{
    QString bandeiraCartao("");

    QString numeroCartao = cartao.numero();
    qint32 quantidadeDigitos = numeroCartao.length();
    QChar digito1 = numeroCartao.at(0);

    bool ok = false;
    numeroCartao.toLongLong(&ok, 10);
    if (ok) {
        if (digito1 == '4' &&
                (quantidadeDigitos == 13 || quantidadeDigitos == 16)) {
            bandeiraCartao = "visa";
        }
    }

    return bandeiraCartao;
}
