#include <gmock/gmock.h>
#include <QString>
#include "operacoescartao.h"
#include "cartao.h"

TEST(CartaoCredito, BandeiraVisa) {
    OperacoesCartao operacoesCartao;
    Cartao cartao("4111111111111111");

    QString bandeiraCartao = operacoesCartao.qualBandeira(cartao);

    EXPECT_EQ("visa", bandeiraCartao);
}

TEST(CartaoCredito, BandeiraVisaInvalidoComLetra) {
    OperacoesCartao operacoesCartao;
    Cartao cartao("411111111111111a");

    QString bandeiraCartao = operacoesCartao.qualBandeira(cartao);

    EXPECT_EQ("", bandeiraCartao);
}
