include (../src/src.pri)

QMAKE_CXXFLAGS += -fprofile-arcs -ftest-coverage
QMAKE_LDFLAGS += -fprofile-arcs -ftest-coverage
QMAKE_CLEAN += *.gcda *.gcno

QT += testlib 

TARGET = tests
CONFIG += console
CONFIG -= app_bundle

TEMPLATE = app

LIBS += -lgmock -lpthread -lgcov

INCLUDEPATH += $$PWD/../src
DEFINES += SRCDIR=\\\"$$PWD/\\\"

SOURCES += main.cpp \
    identificarcartaotest.cpp
